### Version: 1.0

Información Requerida         | 
-------------                 | -------------
Nombre:                       | 
Objetivo:                     | 
Políticas y estándares:       | 
Áreas de CMMI:                | 
Entrada:                      | 
Salida:                       | 
Patrones que apoya:           | 
Antipatrones que previene:    | 


| Fase          | Descripción   | Actividades  | Encargado     | Areas CMMI    |
| ------------- |-------------  | ------------ | ------------- |-------------- | 
| text          | text          | text         | text          | text          |
| text          | text          | text         | text          | text          |
| text          | text          | text         | text          | text          |


### Bitácora

| Versión        | Cambio Realizdo   | Autor del cambio | Aprobado por   | Fecha de cambio |
| -------------  |--------           | -------          | -------------  | -------------   |
| text           | text              | text             | text           |  text           |